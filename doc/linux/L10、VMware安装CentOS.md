
> 流程偏长，下一步根本点不完；

<br/><p align="center" style="font-size:25px">01</p><br/>

首先，明确下两款软件的版本信息；

- VMware是【VMware-Fusion-13.5.0】
- CentOS是【CentOS-7-x86_64-Minimal-1908】；

VMware用来管理虚拟机系统，安装就不多说了，双击就行，注册码也好找；

打开VMware软件，从主页新建镜像，这里使用本地的镜像包；

![](https://foruda.gitee.com/images/1710757257946868750/872cd257_5064118.png "1.png")

VMware软件会自动的从本地识别到可用的镜像文件；

![](https://foruda.gitee.com/images/1710757275778776600/2ffc3f2e_5064118.png "2.png")

这里选择传统BIOS方式；

![](https://foruda.gitee.com/images/1710757290298220129/fc6ab195_5064118.png "3.png")

这里继续之后，会展示默认的配置信息，直接点击【完成】会弹出虚拟机设置面板，或者先自行关闭该窗口，再右键打开【设置选项】；

![](https://foruda.gitee.com/images/1710757304178069163/a8467e70_5064118.png "4.png")

<br/><p align="center" style="font-size:25px">02</p><br/>

根据使用的需求情况，自行修改CentOS的配置信息，这里调整处理器和内存；

![](https://foruda.gitee.com/images/1710757320454592008/951fc030_5064118.png "5.png")

虚拟机配置修改完成后，选择启动【CentOS-7】，第一次需要先安装；

![](https://foruda.gitee.com/images/1710757334297247844/555a6391_5064118.png "6.png")

<br/><p align="center" style="font-size:25px">03</p><br/>

安装开始之后，都默认下一步执行，系统语言按需选择即可；

![](https://foruda.gitee.com/images/1710757349134061929/f637ed72_5064118.png "7.png")

进行到核心配置这块，个人习惯操作两块配置；

- 【1】配置网络，通常选择自动连接；

![](https://foruda.gitee.com/images/1710757363266313738/0f9f9de9_5064118.png "8.png")

- 【2】配置分区，这里流程比较复杂，截了4张图，操作步骤都在图上标记了；

![](https://foruda.gitee.com/images/1710757377591203125/90640ea4_5064118.png "9.png")

配置好之后，就可以点击系统安装，安装的过程中，再顺手设置【root】用户的登录密码，安装完成之后重启即可。

![](https://foruda.gitee.com/images/1710757391432850290/6baf2deb_5064118.png "10.png")

**编程文档**：https://gitee.com/cicadasmile/butte-java-note
