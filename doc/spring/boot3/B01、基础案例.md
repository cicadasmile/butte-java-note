
> 技术和工具「!喜新厌旧」

# 一、背景

最近在一个轻量级的服务中，尝试了最新的技术和工具选型；

即`SpringBoot3`，`JDK17`，`IDEA2023`，`Navicat16`，虽然新的技术和工具都更加强大和高效，但是适应采坑的过程总是枯燥的；

【环境一览】

![](https://foruda.gitee.com/images/1691917467415627093/d55410ed_5064118.png "1.png")

首先框架主体从`SpringBoot2`升级到`SpringBoot3`，Java基础环境从`JDK8`升级到`JDK17`；

技术升级都到这步了，自然连带着工具都升级到最新版本了，涉及到的其他组件，也会选择与当前框架适应的版本；

至于为何使用`JDK17`，因为是`SpringBoot3`的最低依赖，也和官方的维护周期有关；

![](https://foruda.gitee.com/images/1691917479514781173/980e6f64_5064118.png "2.png")

实际上如果`JDK21`已经发布的话，个人更倾于这个版本，要是没有合适的尝试机会，继续使用`JDK8`也问题不大；

另外开发工具`IDEA2021.2`版本才开始支持`JDK17`，所以如果版本过低的话也需要升级，至于`Navicat16`纯属跟风操作；

# 二、环境搭建

## 1、工程结构

在工程结构上没有什么变化，通过maven组件构建项目，对于入门案例来说，注意框架依赖，启动类，配置文件即可；

![](https://foruda.gitee.com/images/1691917490747746445/f9cad6fb_5064118.png "3.png")

## 2、框架依赖

在该工程中只是`SpringBoot3`框架的简单测试，所以只引入`web`依赖就足够；

```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
        <version>${spring-boot.version}</version>
    </dependency>
</dependencies>
```

至于`Spring`框架和其他组件的依赖，顺着`web`依赖追溯即可，核心的依赖和版本都可以找到；

![](https://foruda.gitee.com/images/1691917501830478294/ca1f6226_5064118.png "4.png")

## 3、环境配置

入门案例，在这里只简单的配置服务器和应用名称；

```yaml
server:
  port: 8082                # 端口号
  tomcat:                   # Tomcat组件
    uri-encoding: UTF-8     # URI编码

spring:
  application:
    name: boot-base
```

# 三、入门案例

## 1、测试接口

提供一个简单的`Get`请求接口，使用了部分JDK新版的语法，如果抛出异常会统一处理；

```java
@RestController
public class BootBaseWeb {

    @GetMapping("/boot/base/{id}")
    public Map<String,String> getInfo (@PathVariable String id){
        if (id.isBlank() || "0".equals(id)){
            throw new RuntimeException("参数ID错误");
        }
        var dataMap = new HashMap<String,String>();
        dataMap.put("id",id);
        dataMap.put("boot","base");
        return dataMap ;
    }
}
```

## 2、全局异常

基于注解`RestControllerAdvice`和`ExceptionHandler`统一异常处理；

```java
@RestControllerAdvice
public class HandlerExe {

    @ExceptionHandler(value = Exception.class)
    public Map<String,String> handler02 (HttpServletRequest request, Exception e){
        var errorMap = new HashMap<String,String>() ;
        errorMap.put("code","500");
        errorMap.put("url",request.getRequestURL().toString());
        errorMap.put("msg",e.getMessage());
        return errorMap ;
    }
}
```

## 3、日志打印

### 3.1 日志配置

在`application.yml`文件中，简单的添加日志配置内容，然后从日志文件或者控制台输出查看相关信息；

```yaml
logging:
  level:
    root: info
  file:
    path: ./
    name: logs/${spring.application.name}.log
  pattern:
    console: "%d{yyyy-MM-dd HH:mm:ss} %contextName [%thread] %-5level %logger- %msg%n"
    file: "%d{yyyy-MM-dd HH:mm:ss} %contextName [%thread] %-5level %logger- %msg%n"
  logback:
    rolling-policy:
      max-history: 7
      max-file-size: 10MB
      total-size-cap: 50MB
```

### 3.2 日志打印

虽然采用的是`logback`组件，但是使用`slf4j`的API即可；

```java
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
public class BootLogWeb {

    private static final Logger LOGGER = LoggerFactory.getLogger(BootBaseWeb.class);

    @GetMapping("/boot/print/log")
    public String printLog (HttpServletRequest request){
        LOGGER.info("remote-host:{}",request.getRemoteHost());
        LOGGER.info("request-uri:{}",request.getRequestURI());
        return request.getServerName() ;
    }
}
```

# 四、打包运行

打包代码工程中的`m1-01-boot-base`子模块，以及其相关的依赖；

```
mvn clean -pl m1-01-boot-base -am -Dmaven.test.skip=true package
```

运行`m1-01-boot-base.jar`服务，并指定相应的端口号为`8080`，然后测试其中相关接口查看日志即可；

```
java -jar m1-01-boot-base.jar --server.port=8080
```

# 五、参考源码

```
文档仓库：
https://gitee.com/cicadasmile/butte-java-note

源码仓库：
https://gitee.com/cicadasmile/butte-spring-parent
```